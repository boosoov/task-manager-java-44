package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyTaskListException extends AbstractException {

    public EmptyTaskListException() {
        super("Error! Task list not exist...");
    }

}
