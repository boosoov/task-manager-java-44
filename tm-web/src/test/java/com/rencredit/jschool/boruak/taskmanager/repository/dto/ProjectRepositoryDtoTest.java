package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.IProjectRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectRepositoryDtoTest {

    @Autowired
    private IProjectRepositoryDTO projectRepository;

    @Autowired
    private IUserRepositoryEntity userRepository;

    private User user;
    
    @Before
    public void setup() {
        projectRepository.deleteAll();
        userRepository.deleteAll();
        user = new User("login", "password");
        userRepository.save(user);
    }

    @Test
    public void testSave() {
        final ProjectDTO project = new ProjectDTO(user.getId(), "name");
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
    }

    @Test
    public void testFindById() {
        final ProjectDTO project = new ProjectDTO(user.getId(), "name");
        projectRepository.save(project);
        final ProjectDTO projectFromBase = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBase);
        assertEquals(project.getId(), projectFromBase.getId());
    }

    @Test
    public void testFindByUserIdAndName() {
        final ProjectDTO project = new ProjectDTO(user.getId(), "name");
        projectRepository.save(project);
        final ProjectDTO projectFromBase = projectRepository.findByUserIdAndName(user.getId(), project.getName());
        assertNotNull(projectFromBase);
        assertEquals(project.getId(), projectFromBase.getId());
    }

    @Test
    public void testFindByUserIdAndId() {
        final ProjectDTO project = new ProjectDTO(user.getId(), "name");
        projectRepository.save(project);
        final ProjectDTO projectFromBase = projectRepository.findByUserIdAndId(user.getId(), project.getId());
        assertNotNull(projectFromBase);
        assertEquals(project.getId(), projectFromBase.getId());
    }

    @Test
    public void testExistById() {
        final ProjectDTO project = new ProjectDTO(user.getId(), "name");
        projectRepository.save(project);
        assertTrue(projectRepository.existsById(project.getId()));
        assertFalse(projectRepository.existsById("34234"));
    }

    @Test
    public void testUpdate() {
        ProjectDTO project = new ProjectDTO(user.getId(), "name");
        projectRepository.save(project);
        final ProjectDTO projectFromBase = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBase);
        assertEquals(project.getName(), projectFromBase.getName());

        project.setName("name2");
        projectRepository.save(project);
        final ProjectDTO projectFromBaseWithAnotherLogin = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBaseWithAnotherLogin);
        assertEquals(project.getId(), projectFromBaseWithAnotherLogin.getId());
        assertEquals(project.getName(), projectFromBaseWithAnotherLogin.getName());
    }

    @Test
    public void testFindAll() {
        projectRepository.save(new ProjectDTO());
        projectRepository.save(new ProjectDTO());
        projectRepository.save(new ProjectDTO());
        assertEquals(3, projectRepository.findAll().size());
    }

    @Test
    public void testFindAllByUserId() {
        projectRepository.save(new ProjectDTO(user.getId(), "name"));
        projectRepository.save(new ProjectDTO(user.getId(), "name"));
        projectRepository.save(new ProjectDTO());
        assertEquals(2, projectRepository.findAllByUserId(user.getId()).size());
    }
    
    @Test
    public void testCount() {
        projectRepository.save(new ProjectDTO());
        projectRepository.save(new ProjectDTO());
        projectRepository.save(new ProjectDTO());
        assertEquals(3, projectRepository.count());
    }

    @Test
    public void testCountAllByUserId() {
        projectRepository.save(new ProjectDTO(user.getId(), "name"));
        projectRepository.save(new ProjectDTO(user.getId(), "name"));
        projectRepository.save(new ProjectDTO());
        assertEquals(2, projectRepository.countAllByUserId(user.getId()));
    }

}
