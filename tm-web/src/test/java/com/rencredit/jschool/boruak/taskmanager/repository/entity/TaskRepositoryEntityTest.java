package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IProjectRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.ITaskRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TaskRepositoryEntityTest {

    @Autowired
    private ITaskRepositoryEntity taskRepository;

    @Autowired
    private IUserRepositoryEntity userRepository;

    @Autowired
    private IProjectRepositoryEntity projectRepository;

    private User user;
    
    @Before
    public void setup() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
        projectRepository.deleteAll();
        user = new User("login", "password");
        userRepository.save(user);
    }

    @Test
    public void testSave() {
        final Task task = new Task(user, "name");
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void testFindById() {
        final Task task = new Task(user, "name");
        taskRepository.save(task);
        final Task taskFromBase = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBase);
        assertEquals(task.getId(), taskFromBase.getId());
    }

    @Test
    public void testExistById() {
        final Task task = new Task(user, "name");
        taskRepository.save(task);
        assertTrue(taskRepository.existsById(task.getId()));
        assertFalse(taskRepository.existsById("34234"));
    }

    @Test
    public void testExistByUserIdAndId() {
        final Task task = new Task(user, "name");
        taskRepository.save(task);
        assertTrue(taskRepository.existsByUserIdAndId(user.getId(), task.getId()));
        assertFalse(taskRepository.existsByUserIdAndId(user.getId(), "34234"));
    }

    @Test
    public void testUpdate() {
        Task task = new Task(user, "name");
        taskRepository.save(task);
        final Task taskFromBase = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBase);
        assertEquals(task.getName(), taskFromBase.getName());

        task.setName("name2");
        taskRepository.save(task);
        final Task taskFromBaseWithAnotherLogin = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBaseWithAnotherLogin);
        assertEquals(task.getId(), taskFromBaseWithAnotherLogin.getId());
        assertEquals(task.getName(), taskFromBaseWithAnotherLogin.getName());
    }

    @Test
    public void testFindAll() {
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        assertEquals(3, taskRepository.findAll().size());
    }

    @Test
    public void testFindAllByUserId() {
        taskRepository.save(new Task(user, "name"));
        taskRepository.save(new Task(user, "name"));
        taskRepository.save(new Task());
        assertEquals(2, taskRepository.findAllByUserId(user.getId()).size());
    }

    @Test
    public void testFindAllByProjectId() {
        taskRepository.save(new Task(user, "name"));
        taskRepository.save(new Task(user, "name"));
        taskRepository.save(new Task());

        Project project = new Project();
        projectRepository.save(project);

        Task task1 = new Task(user, "name");
        task1.setProject(project);
        taskRepository.save(task1);
        Task task2 = new Task(user, "name");
        task2.setProject(project);
        taskRepository.save(task2);
        Task task3 = new Task(user, "name");
        task3.setProject(project);
        taskRepository.save(task3);
        Task task4 = new Task(user, "name");
        task4.setProject(project);
        taskRepository.save(task4);

        assertEquals(4, taskRepository.findAllByProjectId(project.getId()).size());
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndId() {
        final Task task = new Task(user, "name");
        taskRepository.save(task);
        final Task taskFromBase = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBase);
        taskRepository.deleteByUserIdAndId(user.getId(), task.getId());
        final Task taskFromBaseAfterDelete = taskRepository.findById(task.getId()).orElse(null);
        assertNull(taskFromBaseAfterDelete);
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndName() {
        final Task task = new Task(user, "name");
        taskRepository.save(task);
        final Task taskFromBase = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBase);
        taskRepository.deleteByUserIdAndName(user.getId(), task.getName());
        final Task taskFromBaseAfterDelete = taskRepository.findById(task.getId()).orElse(null);
        assertNull(taskFromBaseAfterDelete);
    }

    @Test
    @Transactional
    public void testDeleteAllByUserId() {
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        taskRepository.save(new Task());
        assertEquals(3, taskRepository.findAll().size());
        taskRepository.deleteAll();
        assertEquals(0, taskRepository.findAll().size());
    }

}
