#!/usr/bin/env bash

echo "reboot task-manager-client"

if [ ! -f ./tm-client.pid ]; then
	echo "tm-client pid not found! client is not running!"
	exit 1;
fi

echo 'KILL PROCESS WITH PID '$(cat ./tm-client.pid);
kill -9 $(cat ./tm-client.pid)
rm tm-client.pid


java -jar ../../tm-client.jar
echo $! > ./tm-client.pid
echo "TASK-MANAGER CLIENT IS RUNNING WITH PID "$!
