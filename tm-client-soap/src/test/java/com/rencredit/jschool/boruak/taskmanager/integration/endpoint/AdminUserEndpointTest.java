package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class AdminUserEndpointTest {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private AuthService authService;

    @Before
    public void init() throws EmptyLoginException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
        authService.login("admin", "admin");
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
    }

    @Test
    public void testLockUserByLogin() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception {
        final UserDTO userNotLocked = adminUserEndpoint.getUserByLogin("test");
        assertFalse(userNotLocked.isLocked());
        adminUserEndpoint.lockUserByLogin("test");
        final UserDTO userLocked = adminUserEndpoint.getUserByLogin("test");
        assertTrue(userLocked.isLocked());
    }

    @Test
    public void testUnlockUserByLogin() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception {
        final UserDTO userNotLocked = adminUserEndpoint.getUserByLogin("test");
        assertFalse(userNotLocked.isLocked());
        adminUserEndpoint.lockUserByLogin("test");
        final UserDTO userLocked = adminUserEndpoint.getUserByLogin("test");
        assertTrue(userLocked.isLocked());

        adminUserEndpoint.unlockUserByLogin("test");
        final UserDTO userNotLocked2 = adminUserEndpoint.getUserByLogin("test");
        assertFalse(userNotLocked2.isLocked());
    }

    @Test
    public void testAddUserLoginPasswordRole() throws DeniedAccessException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception, UnknownUserException_Exception {
        adminUserEndpoint.addUserLoginPasswordRole("admin2", "2", Role.ADMIN);
        assertTrue(authService.login("admin2", "2"));
    }

    @Test
    public void testGetUserById() throws EmptyLoginException_Exception,EmptyIdException_Exception {
        final UserDTO userByLogin = adminUserEndpoint.getUserByLogin("test");
        assertEquals("test", userByLogin.getLogin());
        final UserDTO userById = adminUserEndpoint.getUserById(userByLogin.getId());
        assertEquals("test", userById.getLogin());
    }

    @Test
    public void testRemoveUserById() throws EmptyLoginException_Exception, EmptyIdException_Exception {
        final UserDTO userByLogin = adminUserEndpoint.getUserByLogin("test");
        assertEquals("test", userByLogin.getLogin());
        adminUserEndpoint.removeUserById(userByLogin.getId());
        assertNull(adminUserEndpoint.getUserByLogin("test"));
    }

    @Test
    public void testRemoveUserByLogin() throws EmptyLoginException_Exception {
        final UserDTO userByLogin = adminUserEndpoint.getUserByLogin("test");
        assertEquals("test", userByLogin.getLogin());
        adminUserEndpoint.removeUserByLogin(userByLogin.getLogin());
        assertNull(adminUserEndpoint.getUserByLogin("test"));
    }

    @Test
    public void testClearAllUser() throws EmptyLoginException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception {
        adminUserEndpoint.clearAllUser();
        assertEquals(3, adminUserEndpoint.getUserList().size());
    }

    @Test
    public void testGetUserList() throws EmptyLoginException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception {
        adminUserEndpoint.clearAllUser();
        assertEquals(3, adminUserEndpoint.getUserList().size());
        adminUserEndpoint.addUserLoginPasswordRole("admin2", "2", Role.ADMIN);
        assertEquals(4, adminUserEndpoint.getUserList().size());
    }

}
