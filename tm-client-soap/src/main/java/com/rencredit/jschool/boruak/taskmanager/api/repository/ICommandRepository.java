package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public interface ICommandRepository {

    @NotNull
    Map<String, AbstractListener> getTerminalCommands();

    @NotNull
    String[] getCommands();

    @NotNull
    String[] getArgs();

    void putCommand(@NotNull final String name, @NotNull final AbstractListener command);

}
