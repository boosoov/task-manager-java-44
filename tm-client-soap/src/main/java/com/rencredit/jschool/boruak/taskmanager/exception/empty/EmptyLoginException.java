package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyLoginException extends AbstractClientException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
